package main.java.ru.iteco.meetup.a1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Simple {
    public static void main(String[] args) throws InterruptedException {
        Set<Path> inputs = new HashSet<>();
        try (Stream<Path> walk = Files.walk(Paths.get(".\\resources\\testset"))) {
            inputs = walk.filter(Files::isRegularFile).collect(Collectors.toSet());
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Integer> results = new ArrayList<>();
        List<Thread> threads = new ArrayList<>();
        for (Path p : inputs) {
            Thread thread = new Thread(() -> {
                try {
                    int sum = IntStream.of(Files.lines(p).mapToInt(Integer::parseInt).toArray()).sum();
                    synchronized (results) {
                        results.add(sum);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
            thread.start();
            threads.add(thread);
        }
        for (Thread thread : threads) {
            thread.join();
        }
        System.out.println(IntStream.of(results.stream().mapToInt(Number::intValue).toArray()).sum());
    }
}
